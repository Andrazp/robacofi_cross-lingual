python incremental_learning_random.py --train_data_path ../data/arabic/arabic_train.tsv \
                                        --test_data_path ../data/arabic/arabic_internal_test.tsv \
                                        --eval_data_path ../data/arabic/arabic_val.tsv \
                                        --output_dir ../models/arabic_xlmr_final \
                                        --data_column data \
                                        --label_column label \
                                        --tokenizer_file xlm-roberta-base \
                                        --model_file ../models/arabic_int_xlmr_base
python incremental_learning_random.py --train_data_path ../data/german/german_train.tsv \
                                        --test_data_path ../data/german/german_internal_test.tsv \
                                        --eval_data_path ../data/german/german_val.tsv \
                                        --output_dir ../models/german_xlmr_final \
                                        --data_column data \
                                        --label_column labels \
                                        --tokenizer_file xlm-roberta-base \
                                        --model_file ../models/german_int_xlmr_base
python incremental_learning_random.py --train_data_path ../data/english/english_train.tsv \
                                        --test_data_path ../data/english/english_test.tsv \
                                        --eval_data_path ../data/english/english_val.tsv \
                                        --output_dir ../models/english_xlmr_final \
                                        --data_column data \
                                        --label_column label \
                                        --tokenizer_file xlm-roberta-base \
                                        --model_file ../models/english_int_xlmr_base
python incremental_learning_random.py --train_data_path ../data/slovenian/slo_train_binarized.tsv \
                                        --test_data_path ../data/slovenian/slo_internal_test_binarized.tsv \
                                        --eval_data_path ../data/slovenian/slo_val_binarized.tsv \
                                        --output_dir ../models/slovenian_xlmr_final \
                                        --data_column data \
                                        --label_column label \
                                        --tokenizer_file xlm-roberta-base \
                                        --model_file ../models/slovenian_int_xlmr_base
python incremental_learning_random.py --train_data_path ../data/croatian/cro_train.tsv \
                                        --test_data_path ../data/croatian/cro_internal_test.tsv \
                                        --eval_data_path ../data/croatian/cro_val.tsv \
                                        --output_dir ../models/croatian_xlmr_final \
                                        --data_column text_a \
                                        --label_column label \
                                        --tokenizer_file xlm-roberta-base \
                                        --model_file ../models/coatian_int_xlmr_base
