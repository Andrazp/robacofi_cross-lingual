python intermediate_learning_leave_one_out_random.py --leave_out_language arabic \
                                                        --output_dir ../models/arabic_int_xlmr_base \
                                                        --model_file xlm-roberta-base
python intermediate_learning_leave_one_out_random.py --leave_out_language croatian \
                                                        --output_dir ../models/coatian_int_xlmr_base \
                                                        --model_file xlm-roberta-base
python intermediate_learning_leave_one_out_random.py --leave_out_language slovenian \
                                                        --output_dir ../models/slovenian_int_xlmr_base \
                                                        --model_file xlm-roberta-base
python intermediate_learning_leave_one_out_random.py --leave_out_language english \
                                                        --output_dir ../models/english_int_xlmr_base \
                                                        --model_file xlm-roberta-base
python intermediate_learning_leave_one_out_random.py --leave_out_language german \
                                                        --output_dir ../models/german_int_xlmr_base \
                                                        --model_file xlm-roberta-base
